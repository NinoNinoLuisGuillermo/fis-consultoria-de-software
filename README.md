# La consultoria 
```plantuml
@startmindmap
+[#B0FE3F] La consultoria: una profesión \n éxito presente y futuro 
**_ quiere 
+++[#CDFD86] Dar una imagen del mundo laboral\n y\n aclarar cualquier tipo de dudas
++++[#CDFD86] la consultoria es un servicio profesional y especializado\n en áreas prestado por \n empresas con la capacidad de elaborar complejos sistemas 
*****_ 3 caracteristicas fundamentales
++++++[#CDFD86] -Conocimiento fundamentales\n-Conocimiento técnico\n-Capacidad de plantear soluciones
++[#74FE8D] La consultoria ofrece los siguientes servicios 
+++ Consultoria
++++[#88FD9D] *Reingenieria de procesos\n*Optimizar atendiendo a los cambios\n*Oficina de proyectos
*****_ Por ejemplo 
++++++[#88FD9D] SentimentalAnalysis 
+++++++[#88FD9D] Elabora una relación de los clientes con tu empresa\n para tener un aproximado de los sentimientos que tienen a la empresa 
+++ Integración
++++[#88FD9D] *Desarrollo a medida\n*Infraestructuras\n*Soluciones de mercado
*****_ Por ejemplo
++++++[#88FD9D] Desarrollo de conectores con redes sociales
+++ Externalización 
++++[#88FD9D] *Gestión de aplicaciones\n*Servicios SAQ\n*Procesos de negocio
*****_ Por ejemplo
++++++[#88FD9D] Operación comunity manager
+++++++[#88FD9D] Gestión de la relación que utilizan las empresas\n para estar conectados con las redes sociales 
++[#85BCFD] también hay servicios que están más relacionados con \n el ámbito del negocio 
***_ Como 
++++[#85BCFD] Servicios de estrategía 
+++++[#85BCFD] Definicion de modelos organizativos
+++++[#85BCFD] Definicion de modelos de negocios 
+++++[#85BCFD] Planes de medios 
+++++[#85BCFD] Gestión de campañas
--[#FC7D7D] Profesion de futuro 
---[#FC7D7D] La consultoria es un modo de vida que permite alcanzar\n la felicidad profesional al ser una profesión competitiva 
--[#FD7F3C] La consultoria tiene un papel clave como proveedor\n de servicios de habilitación digital 
---[#FD7F3C] Marcaran grandes iniciativas\n de futuro que se marcan como tendencias 
----_ como
-----[#FD7F3C] *Transformación digital \n *Indutria 4.0 \n *Smart cities
------[#FD7F3C] Cualquiera de estos ámbitos\n requiere trabajar\n en 3 tipos de actividades 
-------[#FD7F3C] Aplicaciones de gestión intraempresa
--------[#FD7F3C] Soluciones de negocio
--------[#FD7F3C] soluciones de inteligencia 
--------[#FD7F3C] plataformas colaborativas
-------[#FD7F3C] Comunicaciones y tratamiento de datos
--------[#FD7F3C] Ciberseguridad
--------[#FD7F3C] computacion y cloud
--------[#FD7F3C] conectividad y movilidad
-------[#FD7F3C] Hibridacion mundo fsico y digital 
--------[#FD7F3C] Impresion 3D
--------[#FD7F3C] Róbotica avanzada
--------[#FD7F3C] Sensores y sistemas embebidos
--[#E767FE] La consultoria exige 
---[#E767FE] Buenos profesionales 
----_ con 
-----[#E767FE] Formacion y experiencia 
-----[#E767FE] Capacidad de trabajo y evolución
----_ capaces de 
-----[#E767FE] Colaborar en un proyecto común
-----[#E767FE] Trabajar en equipo 
-----[#E767FE] Ofrecer honestidad y sinceridad
--[#67D9FE] Se requiere
---[#67D9FE] Proactividad
----[#67D9FE] impresendible en consultoria
----[#67D9FE] Implica iniciatica y anticipación 
---[#67D9FE] Voluntad de mejora
----[#67D9FE] Actitud activa en contruccion de empresa
----[#67D9FE] Aportacion de ideas de mejora 
---[#67D9FE] Resposabilidad 
----[#67D9FE] Asumir los trabajos para hacerlos 
----[#67D9FE] No devolver nuevos problemas
---[#67D9FE] Disponibilidad total 
----[#67D9FE] Disponibilidad de trabajar las horar que nos exiga el proyecto 
---[#67D9FE] nuevos retos
----[#67D9FE] Actuar por encima de los propios conocimientos 
---[#67D9FE] Viajes 
----[#67D9FE] Movilidad geografica 
--[#67FE92] Su jerarquía normalmente es la siguiente 
---[#67FE92] Junior
----[#67FE92] Trabajo en equipo y gestión de tiempo 
----[#67FE92] Entrevistas eficaces 
---[#67FE92] Senior
----[#67FE92] Motivación y liderazgo 
---[#67FE92] Gerente
----[#67FE92] Técnicas comerciales
----[#67FE92] Negociación empresa 
@endmindmap
```

# Consultoria de software
```plantuml
@startmindmap
+[#84FFD4] Consultoría de software
++[#8494FF] Una de las actividades que se lleva acabo en una consultoría\n de tecnología es el desarrollo de software  
++[#B679FF] La consultoria de software sigue un proceso el cual es 
+++[#B679FF] Necesidad del cliente 
+++[#B679FF] Concultoría con el cliente para saber sus necesidades 
+++[#B679FF] Estudio de viabilidad
++++[#B679FF] Qué se refiere a que la consultoria revisa si es viable\n la elaboracion del software
+++[#B679FF] Diseño funcional
++++[#B679FF] Manejo de la información además de elaborar un prototipo 
+++[#B679FF] Diseño técnico 
++++[#B679FF] Donde se contruye el software
+++[#B679FF] Instalar o subir le software dependiendo de lo que\n requiera el proyecto
++[#84FF8F] Los primeros pasos para la elaboración de un software 
***_ son
++++[#84FF8F] Crear la  parte de la infraestructura 
+++++[#84FF8F] hardware 
+++++[#84FF8F] servidores
+++++[#84FF8F] habilitar la infraestructura\n de comunicaciónes 
++++[#84FF8F] elaborar el software a la medida del cliente 
++++[#84FF8F] Crear un prototipo para que el cliente \n quiere hacer cambios
++++[#84FF8F] Finalmente implementar el software en los equipos\n de la empresa o en su caso de que sea aplicación\n publicarla 
++[#EDFF95] Las tendencias actuales en el desarrollo\n de un sistema de información 
+++[#EDFF95] Cambiar el concepto de lo que era la infraestructura de una empresa 
+++[#EDFF95] Ahora se almacena en la nube 
++++[#EDFF95] La nube 
*****_ es
++++++[#EDFF95] un servidor que es trabajado por grandes empresas las cuales\n se encargaran de la seguridad de tu información\n por lo que ya se saltarán unas tareas 
+++[#EDFF95] Se almacenan millones de datos 
****_ un ejemplo 
+++++[#EDFF95] Las redes sociales tienen todos los datos de sus usuarios en \n servidores en la nube 
+++[#EDFF95] Ya se utilizan base de datos no relacionales
++++[#EDFF95] Esto nos ayuda a encontrar cosas más facilmente ya que\n almacena datos y los procesa, lo que auda a saber\n lo que el usuario quiere 
+++[#EDFF95] también tiene sus desventajas 
****_ como
+++++[#EDFF95] Los servidores dependiendo de la zona de donde los estés contratando\n no se rigen por la misma ley de tu país si no se rigen\n sobre las leyes del paises de los servidores 
@endmindmap
```
#Aplicación de la ingeniería de software

```plantuml
@startmindmap
+[#FD8A8A] Aplicación de la ingeniría de software 
++[#FBBE70] Una empresa requiere de ciertos principios 
***_ como 
++++[#FBBE70] Conocimiento
++++[#FBBE70] Innovación
++++[#FBBE70] Especialización
++++[#FBBE70] Calidad
++++[#FBBE70] ayudar a los clientes
*****_ a
++++++[#FBBE70] Definir
++++++[#FBBE70] Implementar
++++++[#FBBE70] Evolucionar
++[#FFFF4E]  Soluciones RT
+++[#FFFF4E] Son soluciones que pretenden cubrir todos \n todos los procesos de negocio de una empresa
++++[#FFFF4E] Es configurable
+++[#FFFF4E] Cada uno se adapta según las necesidades del negocio 
+++[#FFFF4E] Framework de desarrollo 
++++[#FFFF4E] Se desarrollan cosas especificas que necesite la empresa 
++[#94FF4E] Para empezar un proyecto para un cliente
***_ se divide en 3 fases 
++++[#94FF4E] Analisís 
++++[#94FF4E] Contrucción
++++[#94FF4E] Puebas y formación
***_ se necesita
++++[#94FF4E] Tener visión global de todo lo que hace la empresa 
++++[#94FF4E] Ver cada uno de los procesos que ejecuta cada día a día 
++++[#94FF4E] Identificando subprocesos o tareas
++++[#94FF4E] Se elabora el documento requisito funcional 
++++[#94FF4E] Se elaboro el analisís funcional 
+++++[#94FF4E] Donde viene todo lo que quiere el cliente 
++++[#94FF4E] Plan de proyecto
+++++[#94FF4E] Tareas a realizar y quien las realizará 
+++++[#94FF4E] Este proyecto debe aprobarlo el cliente 
++++[#94FF4E] Diseño técnico
+++++[#94FF4E] De las tareas que se harán, como serán realizadas 
++++++[#94FF4E] Programación
++++++[#94FF4E] Parametrización
@endmindmap
```
